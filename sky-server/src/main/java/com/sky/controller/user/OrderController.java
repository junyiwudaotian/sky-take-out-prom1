package com.sky.controller.user;

import com.sky.dto.OrdersSubmitDTO;
import com.sky.result.Result;
import com.sky.service.OrderService;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j@RestController
@RequestMapping("/user/order")
@Api(tags = "订单控制器")
public class OrderController {
    @Resource
    private OrderService orderService;
    @ApiOperation("订单提交")
    @PostMapping("/submit")
    public Result<OrderSubmitVO> saveOrder(@RequestBody OrdersSubmitDTO ordersSubmitDTO){
        log.info("提交订单 {}",ordersSubmitDTO);
        OrderSubmitVO orderVO=orderService.saveOrder(ordersSubmitDTO);
        return Result.success(orderVO);
    }
}
