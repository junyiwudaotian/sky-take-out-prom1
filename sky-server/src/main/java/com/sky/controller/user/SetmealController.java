package com.sky.controller.user;

import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController("userSetmealController")
@Api(tags = "用户套餐控制器")
@RequestMapping("/user/setmeal")
public class SetmealController {
    @Resource
    private SetmealService setmealService;
    @ApiOperation("套餐展示")
    @GetMapping("list")
    public Result<List<SetmealVO>> showSetmeal(@RequestParam Long categoryId){
        log.info("[套餐展示 {}]",categoryId);
        List<SetmealVO> setmealVOList =setmealService.showSetmeal(categoryId);
        return Result.success(setmealVOList);
    }
    @ApiOperation("根据套餐id展示菜品")
    @GetMapping("/dish/{id}")
    public Result<List<DishVO>> showDishBySetmealId(@PathVariable Long id)
    {
        log.info("[根据套餐id查询菜品 {}]" ,id);
        List<DishVO> dishVOList=setmealService.showDishBySetmealId(id);
        return Result.success(dishVOList);
    }
}
