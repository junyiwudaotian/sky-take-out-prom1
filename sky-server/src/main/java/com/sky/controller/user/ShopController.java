package com.sky.controller.user;

import com.sky.constant.ShopConstant;
import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "店铺营业状态控制器")
@RestController("userShopController")
@RequestMapping("/user/shop")
@Slf4j
public class ShopController {
    @Resource
    private RedisTemplate redisTemplate;
    @GetMapping("/status")
    @ApiOperation("传给用户的信息")
    public Result<Integer> sendShop() {
        Object status = redisTemplate.opsForValue().get(ShopConstant.KEY);
        return status != null ? Result.success((Integer) status) : Result.success(0);
    }
}
