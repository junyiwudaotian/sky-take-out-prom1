package com.sky.controller.user;


import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "购物车控制器")
@RestController
@RequestMapping("/user/shoppingCart")
@Slf4j
public class ShoppingCardController {
    @Resource
    private ShoppingCartService shoppingCartService;
    @ApiOperation("添加商品到购物车")
    @PostMapping("/add")
    public Result<String> addCommodity(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("[添加商品至购物车 {}]",shoppingCartDTO);
        Integer tag =shoppingCartService.addCommodity(shoppingCartDTO);
        return tag>0?Result.success("购物车添加成功"):Result.error("购物车添加失败");
    }
    @ApiOperation("购物车展示")
    @GetMapping("/list")
    public Result<List<ShoppingCart>> selectByUserId(){

        log.info("[购物车展示]");
        List<ShoppingCart> shoppingCartList=shoppingCartService.selectByUserId();
        return Result.success(shoppingCartList);
    }
    @ApiOperation("清空购物车")
    @DeleteMapping("/clean")
    public Result<String> deleteByUserId(){
        log.info("[清空购物车]");
        Integer tag=shoppingCartService.deleteByUserId();
        return tag>0?Result.success("清空成功"):Result.error("清空失败");
    }
    @ApiOperation("单个删除商品")
    @PostMapping("/sub")
    public Result<String> selectByOptions(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("[单个删除商品 {}]",shoppingCartDTO);
        Integer tag =shoppingCartService.deleteByOptions(shoppingCartDTO);
        return tag>0?Result.success("删除成功"):Result.error("删除失败");
    }
}
