package com.sky.controller.user;

import com.sky.dto.UserLoginDTO;
import com.sky.result.Result;
import com.sky.service.UserService;
import com.sky.vo.UserLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "用户控制器")
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Resource
    private UserService userService;
    @ApiOperation("用户登录操作")
    @PostMapping("/user/login")
    public Result<UserLoginVO> weiXinLogin(@RequestBody UserLoginDTO userLoginDTO){
        log.info("[用户微信登录操作 {}]",userLoginDTO);
        UserLoginVO userLoginVO=userService.weiXinLogin(userLoginDTO);
        return Result.success(userLoginVO);

}}
