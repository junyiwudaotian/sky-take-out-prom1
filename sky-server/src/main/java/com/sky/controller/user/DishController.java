package com.sky.controller.user;

import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController("userDishController")
@Api(tags = "用户菜品控制器")
@RequestMapping("/user/dish")
public class DishController {
    @Resource
    private DishService dishService;
    @ApiOperation("菜品展示")
@GetMapping("/list")
    public Result<List<DishVO>> showDish(@RequestParam Long categoryId){
        log.info("[菜品展示 {}]",categoryId);
        List<DishVO> dishVOList=dishService.showDish(categoryId);
        return Result.success(dishVOList);
    }
}
