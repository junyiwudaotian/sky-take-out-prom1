package com.sky.controller.user;

import com.sky.entity.Category;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController("userCategoryController")
@Api(tags = "用户分类控制器")
@RequestMapping("/user/category")
public class CategoryController {
    @Resource
    private CategoryService categoryService;
    @ApiOperation("分类展示")
    @GetMapping("/list")
    public Result<List<Category>>  showCategory(){
        log.info("[展示分类]");
        List<Category> category=categoryService.showCategory();
        return Result.success(category);
    }
}
