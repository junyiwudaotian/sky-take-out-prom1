package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController("adminDishController")
@RequestMapping("/admin/dish")
@Api(tags = "菜品控制器")
public class DishController {
    @Resource
    private DishService dishService;

    @DeleteMapping
    @ApiOperation("批量删除")
    public Result<String> deleteById(@RequestParam List<Long> ids) {
        log.info("[根据id批量删除 ids{}]", ids);
        Integer tag = dishService.deleteById(ids);
        if ((tag > 0)) return Result.success("删除成功");
        return Result.error("删除失败");
    }

    @PostMapping
    @ApiOperation("新增菜品")
    public Result<String> addDish(@RequestBody DishDTO dishDTO) {
        log.info("[新增菜品 {}]", dishDTO);
        Integer tag = dishService.addDish(dishDTO);
        if (tag > 0) return Result.success("新增成功");
        return Result.error("新增失败");
    }

    @GetMapping("/page")
    @ApiOperation("分页查询")
    public Result<PageResult> selectByLimit(DishPageQueryDTO dishPageQueryDTO) {
        log.info("[分页查询 {}]", dishPageQueryDTO);
        PageResult pageResult = dishService.selectByLimit(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    @GetMapping("/list")
    @ApiOperation("根据id查询")
    public Result<List<Dish>> selectByCategoryId(Long categoryId) {
        log.info("[新增菜品 {}]", categoryId);
        List<Dish> dishList = dishService.selectByCategoryId(categoryId);
        return Result.success(dishList);
    }
    @GetMapping("{id}")
    @ApiOperation("根据id查询菜品信息")
    public Result<DishVO> selectById(@PathVariable Long id){
        log.info("[根据id查询菜品 {}]",id);
        DishVO dishVO=dishService.selectById(id);
        return Result.success(dishVO);
    }
    @PutMapping
    @ApiOperation("修改菜品信息")
    public Result<String> updateDish(@RequestBody DishDTO dishDTO){
        log.info("[修改菜品信息 {}]",dishDTO);
        Integer tag=dishService.updateDish(dishDTO);
        return tag>0?Result.success("修改成功"):Result.error("修改失败");
    }
    @PostMapping("/status/{status}")
    @ApiOperation("起售与关停")
    public Result<String> openAndClose(@PathVariable Integer status,Long id){
        log.info("[起售与关停 {} {}]",status,id);
        Integer tag =dishService.openAndClose(status,id);
        return tag>0?Result.success("操作成功"):Result.error("操作失败");
    }
}
