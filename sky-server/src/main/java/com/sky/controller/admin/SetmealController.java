package com.sky.controller.admin;

import com.sky.constant.MessageConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController("adminSetmealController")
@Api(tags = "菜品套餐分类")
@RequestMapping("/admin/setmeal")
public class SetmealController {
    @Resource
    private SetmealService setmealService;
    @PostMapping
    @ApiOperation("新增菜品套餐")
    public Result<String> addSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("[新增菜品套餐 {}]",setmealDTO);
        Integer tag=setmealService.addSetmeal(setmealDTO);
        if (tag>0)return Result.success("新增成功");
        return Result.error("新增失败");
    }

    @GetMapping("/page")
    @ApiOperation("套餐分页查询")
    public Result<PageResult> selectByLimit(SetmealPageQueryDTO setmealPageQueryDTO){
        log.info("[分页查询 {}]",setmealPageQueryDTO);
        PageResult pageResult=setmealService.selectByLimit(setmealPageQueryDTO);
        return Result.success(pageResult);
    }
    @PostMapping("/status/{status}")
    @ApiOperation("启用与停用")
    public Result<String> useAndClose(@PathVariable Integer status,
                                      Long id){
        log.info("[起售与停用 {} {}]",status,id);
        Integer tag = setmealService.useAndClose(status,id);
        if (tag==-1)return Result.error(MessageConstant.SETMEAL_ENABLE_FAILED);
        if (tag>0)return Result.success("操作成功");
        return Result.error("操作失败");
    }
    @DeleteMapping
    @ApiOperation("根据id批量删除")
    public Result<String> deleteById(@RequestParam List<Long> ids){
        log.info("[根据id批量删除 {}]",ids);
        Integer tag=setmealService.deleteById(ids);
        if (tag>0)return Result.success("删除成功");
        return Result.error("删除失败");
    }
    @GetMapping("/{id}")
    @ApiOperation("根据id查询套餐信息")
    public Result<SetmealVO> selectById(@PathVariable Long id){
        log.info("[根据id查询套餐信息 {}]",id);
        SetmealVO setmealVO=setmealService.selectById(id);
        return Result.success(setmealVO);
    }
    @PutMapping
    @ApiOperation("修改套餐信息")
    public Result<String> updateSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("[修改套餐信息 {}]",setmealDTO);
        Integer tag=setmealService.updateSetmeal(setmealDTO);
        if (tag>0)return Result.success("修改成功");
        return Result.error("修改失败");
    }

}
