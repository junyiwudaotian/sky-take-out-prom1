package com.sky.controller.admin;

import com.sky.constant.ShopConstant;
import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
@Api(tags = "店铺营业状态控制器")
@RestController("adminShopController")
@RequestMapping("/admin/shop")
@Slf4j
public class ShopController {
    @Resource
    private RedisTemplate redisTemplate;

    @ApiOperation("传给redis数据库的信息")
    @PutMapping("/{status}")
    public Result<String> setShop(@PathVariable Integer status) {
        log.info("[将数据保存到redis中 status{}]", status);
        redisTemplate.opsForValue().set(ShopConstant.KEY, status);
        return Result.success();
    }

    @GetMapping("/status")
    @ApiOperation("传给前端的信息")
    public Result<Integer> sendShop() {
        Object status = redisTemplate.opsForValue().get(ShopConstant.KEY);
        return status != null ? Result.success((Integer) status) : Result.success(0);
    }
}
