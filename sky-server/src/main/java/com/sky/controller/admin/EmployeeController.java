package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import com.sky.vo.EmployeeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理
 */
@RestController
@RequestMapping("/admin/employee")
@Slf4j
@Api(tags = "员工控制器")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;

    @PostMapping("/status/{status}")
    @ApiOperation("根据员工id禁用与启用")
    public Result<String> openAndClose(@PathVariable Integer status,
                                       Long id) {
        log.info("[启用与禁用员工用户 {}  {}]", status, id);
        Integer tag = employeeService.openAndClose(status, id);
        if (tag > 0) return Result.success("操作成功");
        return Result.error("操作失败");

    }

    @GetMapping("/{id}")
    @ApiOperation("根据员工id修改员工信息第一步")
    public Result<EmployeeVO> selectById(@PathVariable Long id) {
        log.info("[修改员工信息第一步:根据员工id查询员工信息 id{}]", id);
        EmployeeVO employeeVO = employeeService.selectById(id);
        return Result.success(employeeVO);
    }

    @PutMapping
    @ApiOperation("根据员工id修改员工信息第二步")
    public Result<String> updateById(@RequestBody EmployeeDTO employeeDTO) {
        log.info("[根据员工id修改信息第二步 employeeDOT{}]", employeeDTO);
        Integer tag = employeeService.updateById(employeeDTO);
        if (tag > 0) return Result.success("修改成功");
        return Result.error("修改失败");
    }


    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> selectByLimit(EmployeePageQueryDTO employeePageQueryDTO) {
        log.info("[分页查询:{}]", employeePageQueryDTO);
        PageResult pageResult = employeeService.selectByLimit(employeePageQueryDTO);
        return Result.success(pageResult);
    }

    @PostMapping
    @ApiOperation("新增员工")
    public Result<String> addEmp(@RequestBody EmployeeDTO employeeDTO) {
        log.info("[新增员工 {}]", employeeDTO);
        Integer tag = employeeService.addEmp(employeeDTO);
        if (tag > 0) return Result.success("新增成功");
        return Result.error("新增失败");
    }

    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @ApiOperation("员工登录")
    @PostMapping("/login")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //登录成功后，生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),
                jwtProperties.getAdminTtl(),
                claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 退出
     *
     * @return
     */
    @PostMapping("/logout")
    public Result<String> logout() {
        return Result.success();
    }

}
