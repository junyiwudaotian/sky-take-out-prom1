package com.sky.aop;

import com.sky.anno.AutoFill;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Aspect
@Slf4j
@Component
public class AutoFillAspect {
    @Before("@annotation(com.sky.anno.AutoFill)")
    public void autoFill(JoinPoint joinPoint){
        log.info("[公共字段切面   开始运行...........]");
        /*Method[] declaredMethods = joinPoint.getTarget().getClass().getDeclaredMethods();
        for (int i = 0; i < declaredMethods.length; i++) {
            Object[] args = joinPoint.getArgs();
            Object entryObject = args[0];
            OperationType value = declaredMethods[i].getDeclaredAnnotation(AutoFill.class).value();
            try {
                if (value==OperationType.INSERT){
                    log.info("[新增切面......]");
                    entryObject
                            .getClass()
                            .getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class)
                            .invoke(entryObject,LocalDateTime.now());
                    entryObject
                            .getClass()
                            .getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class)
                            .invoke(entryObject,LocalDateTime.now());
                    entryObject
                            .getClass()
                            .getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class)
                            .invoke(entryObject, BaseContext.getCurrentId());
                    entryObject
                            .getClass()
                            .getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class)
                            .invoke(entryObject, BaseContext.getCurrentId());
                }else if (value==OperationType.UPDATE){
                    log.info("[更新切面......]");
                    entryObject
                            .getClass()
                            .getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class)
                            .invoke(entryObject,LocalDateTime.now());
                    entryObject
                            .getClass()
                            .getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class)
                            .invoke(entryObject, BaseContext.getCurrentId());
                }
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }*/
        OperationType value =((MethodSignature)joinPoint
                .getSignature())
                .getMethod()
                .getAnnotation(AutoFill.class)
                .value();
        Object[] args = joinPoint.getArgs();
        Object entryObject = args[0];
        try {
            if (value==OperationType.INSERT){
                log.info("[新增切面......]");
                entryObject
                        .getClass()
                        .getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class)
                        .invoke(entryObject,LocalDateTime.now());
                entryObject
                        .getClass()
                        .getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class)
                        .invoke(entryObject,LocalDateTime.now());
                entryObject
                        .getClass()
                        .getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class)
                        .invoke(entryObject, BaseContext.getCurrentId());
                entryObject
                        .getClass()
                        .getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class)
                        .invoke(entryObject, BaseContext.getCurrentId());
            }else{
                log.info("[更新切面......]");
                entryObject
                        .getClass()
                        .getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class)
                        .invoke(entryObject,LocalDateTime.now());
                entryObject
                        .getClass()
                        .getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class)
                        .invoke(entryObject, BaseContext.getCurrentId());
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
