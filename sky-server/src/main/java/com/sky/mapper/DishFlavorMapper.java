package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DishFlavorMapper {
    void insertFlavor(DishFlavor flavor);

    List<DishFlavor> selectByDishId(Long id);

    void deleteById(List<Long> ids);

}
