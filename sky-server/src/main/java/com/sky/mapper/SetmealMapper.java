package com.sky.mapper;

import com.sky.anno.AutoFill;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {

    /**
     * 根据分类id查询套餐的数量
     *
     * @param id
     * @return
     */
    @Select("select count(id) from setmeal where category_id = #{categoryId}")
    Integer countByCategoryId(Long id);

    @AutoFill(OperationType.INSERT)
    Integer addSetmeal(Setmeal setmeal);

    List<Setmeal> selectByLimit(SetmealPageQueryDTO setmealPageQueryDTO);

    Integer deleteById(List<Long> ids);

    @AutoFill(OperationType.UPDATE)
    Integer useAndClose(Setmeal setmeal);

    SetmealVO selectById(Long id);
@AutoFill(OperationType.UPDATE)
    Integer updateSetmeal(Setmeal setmeal);

    List<Setmeal> showSetmeal(Long categoryId);

}
