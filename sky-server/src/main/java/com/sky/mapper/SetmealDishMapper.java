package com.sky.mapper;


import com.sky.entity.SetmealDish;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper

public interface SetmealDishMapper {

    void addSetmealDish(SetmealDish setmealDish);

    void deleteById(List<Long> ids);

    List<SetmealDish> selectById(Long setmealId);


    List<Long> selectByDishId(Long dishId);

    List<Long> selectBySetmealId(Long SetmealId);



}
