package com.sky.mapper;

import com.sky.anno.AutoFill;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     *
     * @param categoryId
     * @return
     */
    @Select("select count(id) from dish where category_id = #{categoryId}")
    Integer countByCategoryId(Long categoryId);

    @AutoFill(OperationType.INSERT)
    Integer insertDish(Dish dish);

    List<DishVO> selectByLimit(DishPageQueryDTO dishPageQueryDTO)
            ;

    Integer deleteById(List<Long> ids);

    List<Dish> selectByCategoryId(Dish dish);

    Dish selectById(Long id);
@AutoFill(OperationType.UPDATE)
    Integer updatetDish(Dish dish);

    Integer openAndClose(Integer status, Long id);

    List<Dish> showDish(Long categoryId);

}
