package com.sky.mapper;


import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    Integer insertShoppingCart(ShoppingCart shoppingCart);

    Integer updateShoppingCart(ShoppingCart shoppingCart);

    ShoppingCart selectById(ShoppingCart shoppingCart);

    List<ShoppingCart> selectByUserId(Long userId);

    Integer deleteByUserId(Long userId);

    Integer deleteByOptions(ShoppingCart shoppingCart);

}
