package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {
    Integer addCommodity(ShoppingCartDTO shoppingCartDTO);

    List<ShoppingCart> selectByUserId();

    Integer deleteByUserId();

    Integer deleteByOptions(ShoppingCartDTO shoppingCartDTO);

}
