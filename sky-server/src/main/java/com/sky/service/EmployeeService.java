package com.sky.service;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;
import com.sky.vo.EmployeeVO;

public interface EmployeeService {

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    Employee login(EmployeeLoginDTO employeeLoginDTO);

    Integer addEmp(EmployeeDTO employeeDTO);

    PageResult selectByLimit(EmployeePageQueryDTO employeePageQueryDTO);

    Integer openAndClose(Integer status, Long id);

    EmployeeVO selectById(Long id);

    Integer updateById(EmployeeDTO employeeDTO);

}
