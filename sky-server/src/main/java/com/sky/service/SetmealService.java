package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {
    Integer addSetmeal(SetmealDTO setmealDTO);

    PageResult selectByLimit(SetmealPageQueryDTO setmealPageQueryDTO);

    Integer deleteById(List<Long> ids);


    Integer useAndClose(Integer status, Long id);

    SetmealVO selectById(Long id);

    Integer updateSetmeal(SetmealDTO setmealDTO);

    List<SetmealVO> showSetmeal(Long categoryId);

    List<DishVO> showDishBySetmealId(Long id);

}
