package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {
    Integer addDish(DishDTO dishDTO);

    PageResult selectByLimit(DishPageQueryDTO dishPageQueryDTO);

    Integer deleteById(List<Long> ids);

    List<Dish> selectByCategoryId(Long categoryId);

    DishVO selectById(Long id);

    Integer updateDish(DishDTO dishDTO);

    Integer openAndClose(Integer status, Long id);

    List<DishVO> showDish(Long categoryId);

}
