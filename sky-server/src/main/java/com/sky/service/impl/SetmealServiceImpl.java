package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;

import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;

import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SetmealServiceImpl implements SetmealService {
    @Resource
    private SetmealMapper setmealMapper;
    @Resource
    private SetmealDishMapper setmealDishMapper;
    @Resource
    private DishMapper dishMapper;
    @Resource
    private DishFlavorMapper dishFlavorMapper;

    @Transactional(rollbackFor = SQLException.class)
    @Override
    public Integer addSetmeal(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);
        Integer tag=setmealMapper.addSetmeal(setmeal);
        for (SetmealDish setmealDish : setmealDTO.getSetmealDishes()) {
            setmealDish.setSetmealId(setmeal.getId());
            setmealDishMapper.addSetmealDish(setmealDish);
        }

        return tag;
    }

    @Override
    public PageResult selectByLimit(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
        Page<Setmeal> page=(Page<Setmeal>) setmealMapper.selectByLimit(setmealPageQueryDTO);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer deleteById(List<Long> ids) {
        for (Long id : ids) {
            if (setmealMapper.selectById(id).getStatus().equals(StatusConstant.ENABLE)){
                throw  new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        }
        Integer tag=setmealMapper.deleteById(ids);
        setmealDishMapper.deleteById(ids);
        return tag;
    }

    @Override
    public Integer useAndClose(Integer status, Long id) {

        Integer tag;
       List<Long> longList=setmealDishMapper.selectBySetmealId(id);
        for (Long aLong : longList) {
            if (dishMapper.selectById(aLong).getStatus()==StatusConstant.DISABLE){
                return tag=-1;
            }
        }
        new Setmeal();
        Setmeal setmeal= Setmeal.builder().id(id).status(status).build();
        tag=setmealMapper.useAndClose(setmeal);
        return tag;
    }

    @Override
    @Transactional(rollbackFor = SQLException.class)
    public SetmealVO selectById(Long id) {
        SetmealVO setmealVO=new SetmealVO();
        BeanUtils.copyProperties(setmealMapper.selectById(id),setmealVO);
        List<SetmealDish> setmealDishList=setmealDishMapper.selectById(id);
       setmealVO.setSetmealDishes(setmealDishList);
        return setmealVO;
    }

    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer updateSetmeal(SetmealDTO setmealDTO) {
        Setmeal setmeal=new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        Integer tag=setmealMapper.updateSetmeal(setmeal);
        List<Long> list=new ArrayList<>();
        list.add(setmealDTO.getId());
        setmealDishMapper.deleteById(list);
        for (SetmealDish setmealDish : setmealDTO.getSetmealDishes()) {
            setmealDish.setSetmealId(setmeal.getId());
            setmealDishMapper.addSetmealDish(setmealDish);
        }
        return tag;
    }

    /**
     * 套餐展示
     * @param categoryId
     * @return
     */
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public List<SetmealVO> showSetmeal(Long categoryId) {
        List<Setmeal> setmealList=setmealMapper.showSetmeal(categoryId);
        List<SetmealVO> setmealVOList=new ArrayList<>();
        for (Setmeal setmeal : setmealList) {
            SetmealVO setmealVO=new SetmealVO();
            BeanUtils.copyProperties(setmeal,setmealVO);
            setmealVO.setSetmealDishes(setmealDishMapper.selectById(setmeal.getId()));
            setmealVOList.add(setmealVO);
        }
        return setmealVOList;
    }

    /**
     * 根据套餐id查询菜品
     * @param id
     * @return
     */
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public List<DishVO> showDishBySetmealId(Long id) {
        List<Long> dishIds = setmealDishMapper.selectBySetmealId(id);
        List<DishVO> dishVOList=new ArrayList<>();
        for (Long dishId : dishIds) {
            Dish dish = dishMapper.selectById(dishId);
            DishVO dishVO=new DishVO();
            BeanUtils.copyProperties(dish,dishVO);
            dishVO.setFlavors(dishFlavorMapper.selectByDishId(dishId));
            dishVOList.add(dishVO);
        }
        return dishVOList;
    }


}
