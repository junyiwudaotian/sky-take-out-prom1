package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DishServiceImpl implements DishService {
    @Resource
    private DishMapper dishMapper;
    @Resource
    private DishFlavorMapper dishFlavorMapper;
    @Resource
    private SetmealDishMapper setmealDishMapper;
    @Resource
    private SetmealMapper setmealMapper;
@Resource
private RedisTemplate<String,Object> redisTemplate;
    @Transactional(rollbackFor = SQLException.class)
    @Override
    public Integer addDish(DishDTO dishDTO) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);
        Integer tag = dishMapper.insertDish(dish);
        log.info("[入库菜品信息 {}]", dish);
        if (dishDTO.getFlavors() != null) {
            for (DishFlavor flavor : dishDTO.getFlavors()) {
                flavor.setDishId(dish.getId());
                dishFlavorMapper.insertFlavor(flavor);
                log.info("[口味信息 {}]", flavor);
            }
        }
        redisTemplate.delete(redisTemplate.keys("dish_*"));
        return tag;
    }
    @Override
    public PageResult selectByLimit(DishPageQueryDTO dishPageQueryDTO) {
        PageHelper.startPage(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());
        List<DishVO> dishVOList = dishMapper.selectByLimit(dishPageQueryDTO);
        for (DishVO dishVO : dishVOList) {
            List<DishFlavor> dishFlavors = dishFlavorMapper.selectByDishId(dishVO.getId());
            if (dishFlavors != null && dishFlavors.size() != 0) {
                dishVO.setFlavors(dishFlavors);
            }
        }
        Page<DishVO> page = (Page<DishVO>) dishVOList;
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Transactional(rollbackFor = SQLException.class)
    @Override
    public Integer deleteById(List<Long> ids) {
        for (Long id : ids) {
            if (dishMapper.selectById(id).getStatus().equals(StatusConstant.ENABLE))
            {
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
            if (setmealDishMapper.selectByDishId(id).size()!=0){
                throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
            }
        }
        //redis作删除
        for (Long id : ids) {
            Long categoryId = dishMapper.selectById(id).getCategoryId();
            String key="dish_"+categoryId;
            if (redisTemplate.opsForValue().get(key)!=null)redisTemplate.delete(key);
        }
        Integer tag = dishMapper.deleteById(ids);
        dishFlavorMapper.deleteById(ids);
        return tag;
    }
    @Override
    public List<Dish> selectByCategoryId(Long categoryId) {
        Dish dish=new Dish().builder().categoryId(categoryId).status(StatusConstant.ENABLE).build();
        return dishMapper.selectByCategoryId(dish);
    }

    @Override
    @Transactional(rollbackFor = SQLException.class)
    public DishVO selectById(Long id) {
        Dish dish=dishMapper.selectById(id);
        List<DishFlavor> dishFlavorList=dishFlavorMapper.selectByDishId(id);
        DishVO dishVO=new DishVO();
        BeanUtils.copyProperties(dish,dishVO);
        dishVO.setFlavors(dishFlavorList);
        return dishVO;
    }

    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer updateDish(DishDTO dishDTO) {
        List<Long> idList=new ArrayList<>();
        idList.add(dishDTO.getId());
        dishFlavorMapper.deleteById(idList);
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);
        Integer tag = dishMapper.updatetDish(dish);
        log.info("[入库菜品信息 {}]", dish);
        if (dishDTO.getFlavors() != null) {
            for (DishFlavor flavor : dishDTO.getFlavors()) {
                dishFlavorMapper.insertFlavor(flavor);
                log.info("[口味信息 {}]", flavor);
            }
        }
        redisTemplate.delete(redisTemplate.keys("dish_*"));
        return tag;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer openAndClose(Integer status, Long id) {
        Integer tag=dishMapper.openAndClose(status,id);
        if (status==StatusConstant.DISABLE){
            List<Long> list=setmealDishMapper.selectByDishId(id);
            for (Long l : list) {
                new Setmeal();
                Setmeal setmeal=Setmeal.builder().id(l).status(status).build();
                setmealMapper.useAndClose(setmeal);
            }
        }
        return tag;
    }

    /**
     * 菜品展示
     * @param categoryId
     * @return
     */
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public List<DishVO> showDish(Long categoryId) {
        String key="dish_"+categoryId;
        List<DishVO> list = (List<DishVO>)redisTemplate.opsForValue().get(key);
        if (list!=null&&list.size()>0) {
            log.info("[从redis中取出]");
            return list;
        }
        List<Dish> dishList=dishMapper.showDish(categoryId);
        List<DishVO> dishVOList=new ArrayList<>();
        for (Dish dish : dishList) {
            DishVO dishVO=new DishVO();
            BeanUtils.copyProperties(dish,dishVO);
            dishVO.setFlavors( dishFlavorMapper.selectByDishId(dish.getId()));
           dishVOList.add(dishVO);
        }
        redisTemplate.opsForValue().set(key,dishVOList);
        log.info("[添加至redis]");
        return dishVOList;
    }
}
