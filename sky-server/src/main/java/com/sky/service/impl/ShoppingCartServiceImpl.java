package com.sky.service.impl;


import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.DishService;
import com.sky.service.SetmealService;
import com.sky.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Resource
    private ShoppingCartMapper shoppingCartMapper;
    @Resource
    private DishService dishService;
    @Resource
    private SetmealService setmealService;

    /**
     * 购物车添加商品
     *
     * @param shoppingCartDTO 商品dto
     * @return Integer
     */
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer addCommodity(ShoppingCartDTO shoppingCartDTO) {
        //1.查询历史购物车
        Long userId = BaseContext.getCurrentId();
        Integer tag;
        if (shoppingCartDTO.getDishId() == null) {
            ShoppingCart shoppingCart = selectById(shoppingCartDTO);
            if (shoppingCart == null) {
                shoppingCart = new ShoppingCart();
                shoppingCart.setName(setmealService.selectById(shoppingCartDTO.getSetmealId()).getName());
                shoppingCart.setUserId(userId);
                shoppingCart.setSetmealId(shoppingCartDTO.getSetmealId());
                shoppingCart.setNumber(1);
                shoppingCart.setAmount(setmealService.selectById(shoppingCartDTO.getSetmealId()).getPrice());
                shoppingCart.setImage(setmealService.selectById(shoppingCartDTO.getSetmealId()).getImage());
                shoppingCart.setCreateTime(LocalDateTime.now());
                tag = shoppingCartMapper.insertShoppingCart(shoppingCart);
            } else {
                int i = shoppingCart.getNumber() + 1;
                shoppingCart.setNumber(i);
                tag = shoppingCartMapper.updateShoppingCart(shoppingCart);
            }

        } else {
            ShoppingCart shoppingCart = selectById(shoppingCartDTO);
            if (shoppingCart == null || !Objects.equals(shoppingCart.getDishFlavor(), shoppingCartDTO.getDishFlavor())) {
                shoppingCart = new ShoppingCart();
                shoppingCart.setName(dishService.selectById(shoppingCartDTO.getDishId()).getName());
                shoppingCart.setUserId(userId);
                shoppingCart.setDishId(shoppingCartDTO.getDishId());
                shoppingCart.setDishFlavor(shoppingCartDTO.getDishFlavor());
                shoppingCart.setNumber(1);
                shoppingCart.setAmount(dishService.selectById(shoppingCartDTO.getDishId()).getPrice());
                shoppingCart.setImage(dishService.selectById(shoppingCartDTO.getDishId()).getImage());
                shoppingCart.setCreateTime(LocalDateTime.now());
                tag = shoppingCartMapper.insertShoppingCart(shoppingCart);
            } else {
                int i = shoppingCart.getNumber() + 1;
                shoppingCart.setNumber(i);
                tag = shoppingCartMapper.updateShoppingCart(shoppingCart);
            }


        }
        return tag;
    }

    /**
     * 根据用户ID展示购物车页面
     *
     * @return
     */
    @Override
    public List<ShoppingCart> selectByUserId() {
        Long userId = BaseContext.getCurrentId();
        return shoppingCartMapper.selectByUserId(userId);
    }

    /**
     * 清空购物车
     * @return
     */
    @Override
    public Integer deleteByUserId() {
        Long userId = BaseContext.getCurrentId();
        return shoppingCartMapper.deleteByUserId(userId);
    }

    /**
     * 选择性删除
     * @param shoppingCartDTO
     * @return
     */
    @Override
    public Integer deleteByOptions(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = selectById(shoppingCartDTO);
        Integer tag;
        if (shoppingCart.getNumber() > 1) {
            Integer number = shoppingCart.getNumber();
            shoppingCart.setNumber(number - 1);
            tag = shoppingCartMapper.updateShoppingCart(shoppingCart);
        } else {
            tag = shoppingCartMapper.deleteByOptions(shoppingCart);
        }
        return tag;
    }

    public ShoppingCart selectById(ShoppingCartDTO shoppingCartDTO) {
        Long userId = BaseContext.getCurrentId();
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUserId(userId);
        shoppingCart.setDishId(shoppingCartDTO.getDishId());
        shoppingCart.setSetmealId(shoppingCartDTO.getSetmealId());
        return shoppingCartMapper.selectById(shoppingCart);
    }
}
